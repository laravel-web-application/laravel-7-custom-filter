## Laravel AdminLTE Demo APPS 

Untuk Melakukan Instalasi Laravel-AdminLTE SBB : 

- lakukan git clone sbb : git clone https://gitlab.com/laravel-web-application/laravel-7-custom-filter.git

- lalu permision folder di server sbb : sudo chmod -R 755 laravel-adminlte ( sesuaikan dengan nama folder di webserver
  anda disini saya menggunakan nama laravel-adminlte)

- lalu izinkan folder storage agar dapat di-write caranya sbb : chmod -R o+w laravel-adminlte/storage

- setelah itu masuk ke directory folder dan install dependency yang dibutuhkan dengan cara : composer install

- lalu copy file .env dari laptop anda (bisa pakai .env default laravel) ke server jika anda deploy ke server jika tidak
  cukup buat di laptop / pc anda...

- Jalakan perintah ini: `php artisan migrate --seed`

- Buka browser favorite anda: http://127.0.0.1:8000/yajra

## Image Screen Shot

JQuery Demo

![JQuery Demo](img/jquery.png "JQuery Demo")

Yajra Demo

![Yajra Demo](img/yajra.png "Yajra Demo")

## Dapatkan Tips Seputar Pemograman DI Blog Saya :

- [Blog Saya : www.adinata.id](https://adinata.id).

## Fitur Demo Yang Terdapat Di Aplikasi ini :

- [Integrasi Dengan Jquery Datatables :](https://adinata.id/laravel/integrasi-laravel-dengan-jquery-datables-adminlte).

## Semoga Bermanfaat

## Salam, 

## Adinata

